package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dto.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback, Integer>{

}
