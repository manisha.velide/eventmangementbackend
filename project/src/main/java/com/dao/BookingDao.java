package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.Booking;



@Service
public class BookingDao {
	@Autowired
	BookingRepository bookingrepos;
	
	@Transactional
	public void register(Booking booking) {
		bookingrepos.save(booking);
	}
	
	@Transactional
	public List<Booking> displayAllBookings(){
		return bookingrepos.findAll();
	}
	
	@Transactional
	public void removeBookingById(int bookingId) {
		bookingrepos.deleteById(bookingId);
	}
	
	@Transactional
	public Booking selectbookingById(int bookingId) {
		return bookingrepos.findById(bookingId).get();
	}
	
	@Transactional
	public void updateBooking(Booking booking) {
		bookingrepos.save(booking);
	}

}
