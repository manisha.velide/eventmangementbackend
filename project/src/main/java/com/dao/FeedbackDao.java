package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.Feedback;

@Service
public class FeedbackDao {
	
	@Autowired
	FeedbackRepository feedbackrepos;
	
	@Transactional
	public void addFeedback(Feedback feedback) {
		feedbackrepos.save(feedback);
	}
	
	@Transactional
	public List<Feedback> displayFeedback(){
		return feedbackrepos.findAll();
	}
	
	@Transactional
	public void deleteFeedback(int feedbackId) {
		feedbackrepos.deleteById(feedbackId);
		
	}
}
