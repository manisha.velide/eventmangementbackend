package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dto.ServiceEvent;
import com.dto.User;

public interface ServiceEventRepository extends JpaRepository<ServiceEvent, Integer>{

	@Query("from ServiceEvent s where s.eventName = :eventName")
	public ServiceEvent getEventByName(@Param("eventName") String eventName);

	@Query("from ServiceEvent s where s.eventId= :eventId")
	public ServiceEvent addEventByUserId(@Param("eventId") User userId);

	@Query(" from ServiceEvent s where s.eventId = :eventId")
	public List<ServiceEvent> findAllEventid(@Param("eventId")int eventId);

	@Query(" from ServiceEvent s where s.eventLocation = :eventLocation")
	public List<ServiceEvent> findAllEventsByLocation(@Param("eventLocation") String eventLocation);

//	@Query(" from ServiceEvent s where s.userId = :userId")
//	public ServiceEvent removeEventByuserId(User userId);

	




}
