package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dto.User;
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query("from User e where e.emailId =:emailId and e.password= :password")
	public User login(@Param("emailId") String emailId ,@Param("password") String password);

	@Query("from User u where u.userName = :userName")
	public User getUserByName(@Param("userName") String userName);

	@Query("from User e where e.emailId = :emailId")
	public User findByEmailId(@Param("emailId") String emailId);

	@Query("select e.password from User e where e.userName = :userName")
	public String userAuthentication(@Param("userName")String userName);
	
	@Query(" from User u where u.emailId = :emailId")
	public boolean existsByEmailId(@Param("emailId") String emailId);

}
