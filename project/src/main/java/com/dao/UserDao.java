package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dto.User;

@Service
public class UserDao {
	@Autowired
	UserRepository userRepos;

	@Transactional
	public List<User> displayAllUsers(){
		return userRepos.findAll();
	}
	
	@Transactional
	public User registerUser(User user) {

		String password =user.getPassword();
		BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
		String hashedPassword =passwordEncoder.encode(password);
		user.setPassword(hashedPassword);
		return userRepos.save(user);
	}
	
	@Transactional
	public User getUserById(int userId) {
		User user=new User();
		return userRepos.findById(userId).get();
	}
	
	@Transactional
	public User getUserByName(String userName) {
		User user=new User();
		User user1=userRepos.getUserByName(userName);
		if(user1 != null) {
			return user1;
		}
		return user;
	}

	@Transactional
	public User insert(User user) {
		// TODO Auto-generated method stub
		return userRepos.save(user);
	}

	@Transactional
	public void deleteUser(int userId) {
		userRepos.deleteById(userId);
	}
	
	@Transactional
	public void removeBasedById(int userId) {
		userRepos.deleteById(userId);
	}
	
	@Transactional
	public User login(String emailId, String password) {
		User user=userRepos.findByEmailId(emailId);
		if(user == null) {
			return null;
		}
		String hashedPassword =user.getPassword();
		BCryptPasswordEncoder passwordEncoder =new BCryptPasswordEncoder();
		if(passwordEncoder.matches(password, hashedPassword)) {
			return user;
		}else {
			return null;
		}
	}
	
	@Transactional
	public String userAuthentication(String userName) {
		return userRepos.userAuthentication(userName);
	}

}
/*
 * public User existsByEmailId(String emailId) { return
 * userRepos.existsByEmailId(emailId); }
 * 
 * @Query(" from User e where e.emailId :emailId")
public User existsByEmailId(@Param("emailId") String emailId);

@Query(" from User p where p.phoneNo = :phoneNo ")
public User existsByPhoneNo(@Param("phoneNo") String phoneNo);
 * public User existsByPhoneNo(String phoneNo) { return
 * userRepos.existsByPhoneNo(phoneNo); }
 */

