package com.Aagana;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.dto.User;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	@Autowired
	UserDao userDao;

	public UserController (UserDao userDao) {
		this.userDao = userDao;
	}

	@GetMapping("/displayAllUsers")
	public List<User> getAllUsers() {
		return userDao.displayAllUsers();
	}

	@PostMapping("/registerUser")
	public User registerUser(@RequestBody User user) {
		String EmailId = user.getEmailId();
		System.out.println(EmailId);
		String subject = "register";
		String message = "Welcome to Aagana Event Management," + "ThankYou for Joining this origanization";
		EmailService.sendEmail(message, subject, EmailId, "puurshothammaddi120@gmail.com");
		User reguser = new User(0, user.getUserName(), user.getFullName(), user.getEmailId(), user.getPhoneNo(),
				user.getencodedPassword(), null);
			return userDao.insert(reguser);
			
	}

	@GetMapping("/getUserById/{userId}")
	public User getUserById(@PathVariable("userId") int userId) {
		return userDao.getUserById(userId);
	}

	@GetMapping("/getUserByName/{userName}")
	public User getUserByName(@PathVariable("userName") String UserName) {
		return userDao.getUserByName(UserName);
	}

	@PutMapping("/updateUser")
	public void updateUser(@RequestBody User user) {
		userDao.insert(user);
	}

	@DeleteMapping("deleteUser/{userId}")
	public void deleteUser(@PathVariable("userId") int userId) {
		userDao.deleteUser(userId);
		System.out.println("Department Deleted Successfully!!!");
	}

	@GetMapping("/login/{userName}/{password}")
	public boolean userAuthentication(@PathVariable("userName") String UserName,
			@PathVariable("password") String password) {
		String encodedPassword = userDao.userAuthentication(UserName);
		return passwordEncoder.matches(password, encodedPassword);
	}
}

//	@PostMapping("/registerUser") 
//	public String registerUser(@RequestBody User user) { 
//		// check if the email ID already exists
//		if(userDao.existsByEmailId(user.getEmailId()) != null) 
//		{ 
//			return "A user with the same email ID already exists"; 
//		}
//	
//		// check if the phone number already exists 
//		if(userDao.existsByPhoneNo(user.getPhoneNo()) != null) { 
//			return	"A user with the same phone number already exists"; 
//		}
//	
//		// send welcome email to the user 
//		String subject ="Welcome to Aagana Event Management"; 
//		String message ="Thank you for joining this organization!"; 
//		EmailService.sendEmail(message,subject, user.getEmailId(), "puurshothammaddi621@gmail.com");
//	
//		// insert the user into the database 
//		User newUser = new User(0,user.getFullName(), user.getEmailId(), user.getPhoneNo(),user.getencodedPassword(), message, null); userDao.insert(newUser);
//	
//		return "User registered successfully"; 
//	}


