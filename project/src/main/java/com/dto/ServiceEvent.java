package com.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name="serviceevent")
public class ServiceEvent {		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int eventId;

	@Column(name="eventName", nullable=false) 
	private String eventName;

	@Column(name="ownerName", nullable=false) 
	private String ownerName;

	@Column(name="ownerEmail", nullable=false) 
	private String ownerEmail;

	@Column(name="ownerNumber",length = 12,nullable=false) 
	private long ownerNumber;

	@Column(name="cost", nullable=false) 
	private double cost;

	@Column(name="serviceType", nullable=false) 
	private String serviceType;

	@Column(name="eventLocation", nullable=false) 
	private String eventLocation;

	@Column(name="eventImage", nullable=false) 
	private String eventImage;

	@Column(name="eventImage1", nullable=false) 
	private String eventImage1;

	@Column(name="eventImage2", nullable=false) 
	private String eventImage2;

	@Column(name="review", nullable=false) 
	private String review;

	@Column(name="descrption", nullable=false) 
	private String descrption;

	public ServiceEvent() {
		super();
	}

	public ServiceEvent(int eventId, String eventName, String ownerName, String ownerEmail, long ownerNumber,
			double cost, String serviceType, String eventLocation, String eventImage, String eventImage1,
			String eventImage2, String review, String descrption) {
		super();
		this.eventId = eventId;
		this.eventName = eventName;
		this.ownerName = ownerName;
		this.ownerEmail = ownerEmail;
		this.ownerNumber = ownerNumber;
		this.cost = cost;
		this.serviceType = serviceType;
		this.eventLocation = eventLocation;
		this.eventImage = eventImage;
		this.eventImage1 = eventImage1;
		this.eventImage2 = eventImage2;
		this.review = review;
		this.descrption = descrption;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerEmail() {
		return ownerEmail;
	}

	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}

	public long getOwnerNumber() {
		return ownerNumber;
	}

	public void setOwnerNumber(long ownerNumber) {
		this.ownerNumber = ownerNumber;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getEventLocation() {
		return eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	public String getEventImage() {
		return eventImage;
	}

	public void setEventImage(String eventImage) {
		this.eventImage = eventImage;
	}

	public String getEventImage1() {
		return eventImage1;
	}

	public void setEventImage1(String eventImage1) {
		this.eventImage1 = eventImage1;
	}

	public String getEventImage2() {
		return eventImage2;
	}

	public void setEventImage2(String eventImage2) {
		this.eventImage2 = eventImage2;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getDescrption() {
		return descrption;
	}

	public void setDescrption(String descrption) {
		this.descrption = descrption;
	}

	@Override
	public String toString() {
		return "ServiceEvent [eventId=" + eventId + ", eventName=" + eventName + ", ownerName=" + ownerName
				+ ", ownerEmail=" + ownerEmail + ", ownerNumber=" + ownerNumber + ", cost=" + cost + ", serviceType="
				+ serviceType + ", eventLocation=" + eventLocation + ", eventImage=" + eventImage + ", eventImage1="
				+ eventImage1 + ", eventImage2=" + eventImage2 + ", review=" + review + ", descrption=" + descrption
				+ "]";
	}


}
/*
 * @ManyToOne
 * 
 * @JoinColumn(name="userId") User user;
 */
