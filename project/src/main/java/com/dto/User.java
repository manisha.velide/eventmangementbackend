package com.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {
	private static final BCryptPasswordEncoder passwordEncoder =new BCryptPasswordEncoder();
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	@Column(name="fullName", nullable=false)
	private String fullName;
	@Column(name="emailId", nullable=false)
	private String emailId;
	@Column(name="userName", nullable=false)
	private String userName;
	@Column(name="phoneNo",length = 12 ,nullable=false)
	private long phoneNo;
	@Column(name="password", nullable=false)
	@JsonIgnore
	private String password;


	public User() {
		super();
	}

	public User(int userId, String fullName, String emailId, String userName, long phoneNo, String password,ServiceEvent serviceevent) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.emailId = emailId;
		this.userName = userName;
		this.phoneNo = phoneNo;
		this.password = password;

	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public String getencodedPassword() {
		return passwordEncoder.encode(password);
	}
	
	public  BCryptPasswordEncoder getPasswordencoder() {
		return passwordEncoder;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", fullName=" + fullName + ", emailId=" + emailId + ", userName=" + userName
				+ ", phoneNo=" + phoneNo + ", password=" + password + "]";
	}

	

	/*
	 * @JsonIgnore
	 * 
	 * @OneToMany(mappedBy="user") List<ServiceEvent> eventlist=new
	 * ArrayList<ServiceEvent>();
	 */	

}
