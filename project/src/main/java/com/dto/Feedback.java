package com.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name="feedback")
public class Feedback {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int feedbackId;
	
	@Column(name="name", nullable=false) 
	private String name;
	
	@Column(name="email", nullable=false) 
	private String email;
	
	@Column(name="phone", nullable=false) 
	private long phone;
	
	@Column(name="text", nullable=false) 
	private long text;

	

}
